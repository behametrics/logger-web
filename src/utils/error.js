/**
 * Handles a sensor-related error occurring in runtime.
 * @param {!Event} event Error event.
 */
export function handleRuntimeError(event) {
  if (event.error.name === 'NotAllowedError') {
    console.log('Permission to access sensor was denied.');
  } else if (event.error.name === 'NotReadableError') {
    console.log('Cannot connect to the sensor.');
  }
}

/**
 * Handles an error occurring when trying to create a sensor instance.
 * @param {!Error} error Construction error.
 */
export function handleConstructionError(error) {
  if (error.name === 'SecurityError') {
    console.log('Sensor construction was blocked by the Feature Policy.');
  } else if (error.name === 'ReferenceError') {
    console.log('Sensor is not supported by the User Agent.');
  } else {
    throw error;
  }
}
