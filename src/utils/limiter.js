/**
 * Class allowing to throttle a data stream.
 */
export class Limiter {
  /**
   * @param {function(!Array): undefined} logEventsFunction Function handling logging input events.
   * @param {number} chunkSize Number of data chunks.
   * @param {number} interval Time interval in which data are collected.
   */
  constructor(logEventsFunction, chunkSize = 60, interval = 1000) {
    this.dataQueue_ = [];
    this.logEventsFunction_ = logEventsFunction;
    this.chunkSize_ = chunkSize;
    this.interval_ = interval;
    this.intervalId_ = null;

    this.initialize();
  }

  /**
   * Starts processing data.
   */
  initialize() {
    if (this.intervalId_ === null) {
      this.intervalId_ = setInterval(() => this.handler_(), this.interval_);
    } else {
      console.error('Limiter is already initialized');
    }
  }

  /**
   * Stops processing data.
   */
  terminate() {
    clearInterval(this.intervalId_);
    this.intervalId_ = null;
    this.processData_(this.dataQueue_);
    this.dataQueue_ = [];
  }

  /**
   * Passes data to the limiter.
   * @param {*} data Data from a stream.
   */
  pushData(data) {
    this.dataQueue_.push(data);
  }

  /**
   * Returns every n-th item of the data array.
   * @private
   * @param {!Array} data Data to process.
   * @param {number} nth Offset from the beginning of `data`.
   */
  getEveryNth_(data, nth) {
    return data.filter((e, i) => i % nth === nth - 1);
  }

  /**
   * Processes every n-th item from the data.
   * @param {!Array} data Data to process.
   */
  processData_(data) {
    const nth = Math.max(Math.floor(data.length / this.chunkSize_), 1);
    this.getEveryNth_(data, nth).forEach((value) => this.logEventsFunction_(value));
  }

  /**
   * Processes data from an internal queue and empties the queue.
   * @private
   */
  handler_() {
    const arrayCopy = [...this.dataQueue_];
    this.dataQueue_ = [];
    this.processData_(arrayCopy);
  }
}
