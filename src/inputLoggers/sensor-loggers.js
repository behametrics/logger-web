import {Limiter} from '../utils/limiter';
import {handleConstructionError, handleRuntimeError} from '../utils/error';
import {InputLogger} from './input-logger';
import {
  INPUT_SENSOR_ACCELEROMETER,
  INPUT_SENSOR_GYROSCOPE,
  INPUT_SENSOR_LINEAR_ACCELERATION,
  INPUT_SENSOR_MAGNETIC_FIELD,
} from './input-names';

/**
 * Class responsible for logging data from the specified sensor.
 * Depending on the availability, the Generic Sensor API (preferred) or the {@link DeviceMotionEvent} is used to log
 * sensor data.
 */
export class SensorLogger extends InputLogger {
  /**
   * @param {!Object} config Logger configuration.
   * @param {function(!Array): undefined} logEventsFunction Function handling logging input events.
   * @param {string} inputName Name of the sensor to be included in logged sensor events as the input name.
   * @param {string} genericApiSensorTypeName Name of the sensor object type for the Generic Sensor API.
   * @param {function(!Sensor): !Object} getSensorContentWithGenericApi Function that returns an object holding sensor
   *     data using the Generic Sensor API.
   * @param {function(!DeviceMotionEvent): !Object} getSensorContentWithDeviceMotion Function that returns an object
   *     holding sensor data using the {@link DeviceMotionEvent}. You may pass `null` to indicate that a sensor is
   *     not applicable for {@link DeviceMotionEvent}.
   */
  constructor(
      config,
      logEventsFunction,
      inputName,
      genericApiSensorTypeName,
      getSensorContentWithGenericApi,
      getSensorContentWithDeviceMotion = null) {
    super(config, logEventsFunction);

    /** Input name for the sensor whose events are logged. */
    this.inputName = inputName;

    this.genericApiSensorTypeName_ = genericApiSensorTypeName;
    this.getSensorContentWithGenericApi_ = getSensorContentWithGenericApi;
    this.getSensorContentWithDeviceMotion_ = getSensorContentWithDeviceMotion;
  }

  /**
   * Starts logging sensor events.
   * @protected
   */
  startLogging() {
    if (window[this.genericApiSensorTypeName_] != null) {
      this.startLoggingWithGenericApi_(this.getSensorConfig_());
    } else if (window.DeviceMotionEvent != null && this.getSensorContentWithDeviceMotion_ != null) {
      this.startLoggingWithDeviceMotion_(this.getSensorConfig_());
    } else {
      console.log(`Input '${this.inputName}' not available`);
    }
  }

  /**
   * Stops logging sensor events.
   * @protected
   */
  stopLogging() {
    if (this.sensor) {
      this.sensor.stop();
      this.sensor.removeEventListener('reading', this.sensorGenericHandler_);
    } else {
      if (this.sensorLimiter) {
        this.sensorLimiter.terminate();
      }
      window.removeEventListener('devicemotion', this.sensorMotionHandler_);
    }
  }

  /**
   * Starts logging sensor data using the Generic Sensor API.
   * @private
   * @param {!Object} sensorConfig Sensor-specific configuration.
   */
  startLoggingWithGenericApi_(sensorConfig) {
    try {
      this.sensor = new window[this.genericApiSensorTypeName_](sensorConfig);
      this.sensor.addEventListener('error', handleRuntimeError);
      this.sensorGenericHandler_ = (event) => {
        return this.logEventsFunction_([this.getSensorEventWithGenericApi(this.sensor, event)]);
      };
      this.sensor.addEventListener('reading', this.sensorGenericHandler_);
      this.sensor.start();
    } catch (error) {
      handleConstructionError(error);
    }
  }

  /**
   * Starts logging sensor data using the {@link DeviceMotionEvent}.
   * @private
   * @param {!Object} sensorConfig Sensor-specific configuration.
   */
  startLoggingWithDeviceMotion_(sensorConfig) {
    this.sensorLimiter = new Limiter(this.logEventsFunction_, sensorConfig.frequency);
    this.sensorMotionHandler_ = (event) => this.sensorLimiter.pushData([this.getSensorEventWithDeviceMotion(event)]);
    window.addEventListener('devicemotion', this.sensorMotionHandler_);
  }

  /**
   * Returns a sensor event using the Generic Sensor API.
   * @param {!Sensor} sensor Raw sensor measurements.
   * @param {!Event} event Event containing additional information such as a timestamp.
   */
  getSensorEventWithGenericApi(sensor, event) {
    return {
      input: this.inputName,
      timestamp: event.timeStamp,
      content: this.getSensorContentWithGenericApi_(sensor),
    };
  }

  /**
   * Returns a sensor event using the {@link DeviceMotionEvent}.
   * @param {!DeviceMotionEvent} event Raw device motion event.
   */
  getSensorEventWithDeviceMotion(event) {
    return {
      input: this.inputName,
      timestamp: event.timeStamp,
      content: this.getSensorContentWithDeviceMotion_(event),
    };
  }

  /**
   * @private
   */
  getSensorConfig_() {
    return this.config_.sensorConfig[this.inputName] || {};
  }
}

/**
 * Class responsible for logging accelerometer events without the contribution of gravity.
 */
export class LinearAccelerationLogger extends SensorLogger {
  /**
   * @param {!Object} config Logger configuration.
   * @param {!function(!Array): undefined} logEventsFunction Function handling logging input events.
   */
  constructor(config, logEventsFunction) {
    super(
        config,
        logEventsFunction,
        INPUT_SENSOR_LINEAR_ACCELERATION,
        'LinearAccelerationSensor',
        (sensor) => ({
          x: sensor.x,
          y: sensor.y,
          z: sensor.z,
        }),
        (event) => ({
          x: event.acceleration.x,
          y: event.acceleration.y,
          z: event.acceleration.z,
        })
    );
  }
}

/**
 * Class responsible for logging accelerometer events with the contribution of gravity.
 */
export class AccelerometerLogger extends SensorLogger {
  /**
   * @param {!Object} config Logger configuration.
   * @param {function(!Array): undefined} logEventsFunction Function handling logging input events.
   */
  constructor(config, logEventsFunction) {
    super(
        config,
        logEventsFunction,
        INPUT_SENSOR_ACCELEROMETER,
        'Accelerometer',
        (sensor) => ({
          x: sensor.x,
          y: sensor.y,
          z: sensor.z,
        }),
        (event) => ({
          x: event.accelerationIncludingGravity.x,
          y: event.accelerationIncludingGravity.y,
          z: event.accelerationIncludingGravity.z,
        })
    );
  }
}

/**
 * Class responsible for logging gyroscope events.
 */
export class GyroscopeLogger extends SensorLogger {
  /**
   * @param {!Object} config Logger configuration.
   * @param {function(!Array): undefined} logEventsFunction Function handling logging input events.
   */
  constructor(config, logEventsFunction) {
    super(
        config,
        logEventsFunction,
        INPUT_SENSOR_GYROSCOPE,
        'Gyroscope',
        (sensor) => ({
          x: sensor.x,
          y: sensor.y,
          z: sensor.z,
        }),
        (event) => ({
          x: event.rotationRate.alpha,
          y: event.rotationRate.beta,
          z: event.rotationRate.gamma,
        })
    );
  }
}

/**
 * Class responsible for logging magnetometer events.
 */
export class MagneticFieldLogger extends SensorLogger {
  /**
   * @param {!Object} config Logger configuration.
   * @param {function(!Array): undefined} logEventsFunction Function handling logging input events.
   */
  constructor(config, logEventsFunction) {
    super(
        config,
        logEventsFunction,
        INPUT_SENSOR_MAGNETIC_FIELD,
        'Magnetometer',
        (sensor) => ({
          x: sensor.x,
          y: sensor.y,
          z: sensor.z,
        })
    );
  }
}
