import {InputLogger} from './input-logger';
import {INPUT_KEYBOARD} from './input-names';

const KEYBOARD_EVENT_TYPES = new Map([
  ['keydown', 'down'],
  ['keyup', 'up'],
]);
const KEYBOARD_UNKNOWN_EVENT_TYPE_PREFIX = 'other_';

/**
 * Class responsible for logging events from keyboard, specifically key presses and releases.
 */
export class KeyboardLogger extends InputLogger {
  /**
   * Starts logging keyboard events.
   * @protected
   */
  startLogging() {
    this.handlerReference_ = this.eventHandler_.bind(this);

    KEYBOARD_EVENT_TYPES.forEach((eventType, rawEventType) => {
      document.addEventListener(rawEventType, this.handlerReference_);
    });
  }

  /**
   * Stops logging keyboard events.
   * @protected
   */
  stopLogging() {
    KEYBOARD_EVENT_TYPES.forEach((eventType, rawEventType) => {
      document.removeEventListener(rawEventType, this.handlerReference_);
    });
  }

  /**
   * Returns an array of keyboard events.
   * @param {KeyboardEvent} event Raw keyboard event.
   */
  getKeyboardEvent(event) {
    return {
      input: INPUT_KEYBOARD,
      timestamp: event.timeStamp,
      content: {
        event_type: KeyboardLogger.getEventType(event),
        key_code: event.code,
        key_name: event.key,
      },
    };
  }

  /**
   * Returns the keyboard event type.
   * @param {KeyboardEvent} event Raw keyboard event.
   */
  static getEventType(event) {
    const eventType = KEYBOARD_EVENT_TYPES.get(event.type);
    return eventType != null ? eventType : `${KEYBOARD_UNKNOWN_EVENT_TYPE_PREFIX}${event.type}`;
  }

  /**
   * Handles keybaord events.
   * @private
   * @param {KeyboardEvent} event Raw keyboard event.
   */
  eventHandler_(event) {
    this.logEventsFunction_([this.getKeyboardEvent(event)]);
  }
}
