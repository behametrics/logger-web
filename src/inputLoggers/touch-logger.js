import {InputLogger} from './input-logger';
import {INPUT_TOUCH} from './input-names';

const TOUCH_EVENT_TYPES = new Map([
  ['touchstart', 'down'],
  ['touchend', 'up'],
  ['touchmove', 'move'],
  ['touchcancel', 'cancel'],
]);
const TOUCH_UNKNOWN_EVENT_TYPE = 'other';

/**
 * Class responsible for logging events describing touch screen interaction.
 */
export class TouchLogger extends InputLogger {
  /**
   * Starts logging touch events.
   * @protected
   */
  startLogging() {
    this.handlerReference_ = this.eventHandler_.bind(this);

    TOUCH_EVENT_TYPES.forEach((eventType, rawEventType) => {
      document.addEventListener(rawEventType, this.handlerReference_);
    });
  }

  /**
   * Stops logging touch events.
   * @protected
   */
  stopLogging() {
    TOUCH_EVENT_TYPES.forEach((eventType, rawEventType) => {
      document.removeEventListener(rawEventType, this.handlerReference_);
    });
  }

  /**
   * Returns an array of touch events.
   * @param {TouchEvent} event Raw touch event.
   */
  getTouchEvents(event) {
    const inputEvents = [];

    for (let i = 0; i < event.changedTouches.length; i++) {
      const touchEventForIdentifier = event.changedTouches.item(i);

      inputEvents.push({
        input: INPUT_TOUCH,
        timestamp: event.timeStamp,
        content: {
          pointer_id: touchEventForIdentifier.identifier,
          event_type: TouchLogger.getEventType(event),
          event_type_detail: TouchLogger.getEventTypeDetail(event),
          x: touchEventForIdentifier.screenX,
          y: touchEventForIdentifier.screenY,
          pressure: touchEventForIdentifier.force,
        },
      });
    }

    return inputEvents;
  }

  /**
   * Returns the touch event type as a string.
   * @param {TouchEvent} event Raw touch event.
   */
  static getEventType(event) {
    const eventType = TOUCH_EVENT_TYPES.get(event.type);
    return eventType != null ? eventType : TOUCH_UNKNOWN_EVENT_TYPE;
  }

  /**
   * Returns a string describing `event.type` in further detail if necessary. If not, `event.type` is returned.
   * @param {TouchEvent} event Raw touch event.
   */
  static getEventTypeDetail(event) {
    const eventTypeDetail = TOUCH_EVENT_TYPES.get(event.type);
    return eventTypeDetail != null ? eventTypeDetail : event.type;
  }

  /**
   * Handles logging of touch events.
   * @private
   * @param {TouchEvent} event Raw touch event.
   */
  eventHandler_(event) {
    this.logEventsFunction_(this.getTouchEvents(event));
  }
}
