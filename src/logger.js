import {getFilledConfigWithDefaults} from './config';
import {postData, getSessionId} from './connection.service';
import {getDeviceType} from './utils/utils';
import {getTimestampNanoseconds, getDocumentStartTimestamp} from './utils/timestamp';
import {getInputNamesAndLoggers} from './inputLoggers/inputs';
import {INPUT_CUSTOM_EVENT, INPUT_METADATA} from './inputLoggers/input-names';
import {Subject} from 'rxjs';
import {bufferTime} from 'rxjs/operators';

/**
 * Class providing high-level API for logging data from pointing devices (such as a mouse) and mobile devices (touch
 * screen and sensors such as an accelerometer).
 * The logged events are sent in batches to the specified backend (via {@link Logger.config}) and can optionally be
 * printed to the console.
 */
export class Logger {
  /**
   * @param {?Object} config Logger configuration. Missing entries are substituted with default entries. If omitted or
   *     `null`, the entire default configuration is used.
   */
  constructor(config = null) {
    /**
     * Object holding logger configuration. The configuration may be modified even during runtime.
     * Note that modifying inputs while the logger is running will have no effect, you need to explicitly invoke
     * {@link stop} and {@link start}.
     */
    this.config = getFilledConfigWithDefaults(config);

    /**
     * The current session ID as a string, or `null` if not initialized by {@link init}.
     * @type {?String}
     */
    this.sessionId = null;

    this.documentStartTimestamp_ = getDocumentStartTimestamp();

    this.eventSendingSubject_ = new Subject();
    this.initEventSendingSubject_();

    this.inputNamesAndLoggers_ = getInputNamesAndLoggers();
    this.inputNamesAndLoggers_.forEach((loggerData, inputName) => {
      this.inputNamesAndLoggers_.get(inputName).logger = new loggerData.LoggerType(
          this.config, this.logInputEvents_.bind(this));
    });

    this.metadataLogger_ = this.inputNamesAndLoggers_.get(INPUT_METADATA).logger;
    this.customEventLogger_ = this.inputNamesAndLoggers_.get(INPUT_CUSTOM_EVENT).logger;
  }

  /**
   * Initializes a new session with unique session ID.
   * @param {!Object} sessionData POST data to be sent.
   */
  async init(sessionData = {}) {
    if (this.config.apiUrl == null) {
      return;
    }

    this.sessionId = await getSessionId(
        sessionData, this.config.apiUrl, this.config.sessionEndpointUrl, this.config.postLoggerHeader);

    console.log('Logging started with session ID:', this.sessionId);

    return this.sessionId;
  }

  /**
   * Starts logging of input events.
   */
  start() {
    const deviceType = getDeviceType();

    this.inputNamesAndLoggers_.forEach((loggerData, inputName) => {
      if (this.config.inputs.includes(inputName) && loggerData.deviceTypes.includes(deviceType)) {
        loggerData.logger.start();
      }
    });
  }

  /**
   * Stops logging of input events.
   */
  stop() {
    this.inputNamesAndLoggers_.forEach((loggerData) => {
      loggerData.logger.stop();
    });
  }

  /**
   * Logs a custom event.
   * @param {!Object} content Contents of the event.
   * @param {string} inputName Input name.
   */
  logCustomEvent(content, inputName = INPUT_CUSTOM_EVENT) {
    this.customEventLogger_.log(content, inputName);
  }

  /**
   * Logs metadata associated with the device in the current session as an input event. Metadata may include the browser
   * name, version, screen properties (such as the width or height) and many more items.
   */
  logMetadata() {
    this.metadataLogger_.log();
  }

  /**
   * Logs the specified input events.
   * If the internal buffer is full, logged events will be sent to the backend.
   * @private
   * @param {!Array} inputEvents Input events to log.
   */
  logInputEvents_(inputEvents) {
    inputEvents.forEach((inputEvent) => {
      inputEvent.timestamp = getTimestampNanoseconds(inputEvent.timestamp, this.documentStartTimestamp_);
      inputEvent.session_id = this.sessionId;

      if (this.config.logToConsole) {
        console.log(inputEvent);
      }

      this.eventSendingSubject_.next(inputEvent);
    });
  }

  /**
   * Initializes the subject responsible for buffering and sending logged events.
   * @private
   */
  initEventSendingSubject_() {
    this.eventSendingSubject_.pipe(
        bufferTime(this.config.batchIntervalMilliseconds, null, this.config.batchSize, null),
    ).subscribe({
      next: (inputEvents) => this.sendLoggedEvents_(inputEvents),
    });
  }

  /**
   * Sends logged data to the backend and clears the internal buffer.
   * @param {!Array} inputEvents Input events to be sent.
   * @private
   */
  sendLoggedEvents_(inputEvents) {
    if (inputEvents.length > 0 && this.config.apiUrl != null) {
      postData(inputEvents, this.config.apiUrl, this.config.dataEndpointUrl, this.config.postLoggerHeader);
    }
  }
}
