#!/bin/sh

if [ -z `git tag -l --points-at HEAD | grep -E 'v?[0-9]+\.[0-9]+\.[0-9]+'` ]
  then
    echo "HEAD is missing version tag"
    exit 1
fi

npm publish --access public
