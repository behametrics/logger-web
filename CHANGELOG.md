# 3.1.1

* Fixed parsing of git tags in the package publishing script.

# 3.1.0

* Added support for logging data from keyboards (key presses and releases), off by default.
* Touch `cancel` event is now a top-level event type rather than being hidden under `other` as an event type detail.
* Flattened contents of metadata events. This allows to properly create CSV columns for each field in the `"content"` object.

# 3.0.0

* Cursor, wheel and touch events are now sent with `"content"` objects instead of arrays to comply with [Behametrics specification](https://gitlab.com/behametrics/specification/blob/release/Specification.md).
* Fixed incorrect formatting of inline code in JSDoc documentation.

# 2.0.0

* Renamed project to Behametrics - Web Logger.
* Standardized format of logged events according to [Behametrics specification](https://gitlab.com/behametrics/specification/blob/release/Specification.md).
* Allowed modifying logger configuration during runtime.
* Logged events are now sent over the network periodically,
  ensuring that events are not lost on page close or reload if the internal buffer is not full.
* Updated `Logger` API for clarity and consistency.
* Custom events and metadata are now logged only if the logger is running.
* Allowed disabling sending events over the network (by setting `apiUrl` to `null`).
* Allowed configuring entries associated with sending events or obtaining a session ID.

# 1.2.2

* Fixed JSDoc documentation generation.

# 1.2.1

* Updated names of fields to maintain compatibility with `behalearn`.

# 1.2.0

* Allowed logging device metadata (such as browser name, version, OS, screen width and height).

# 1.1.4

* Improved error handling if obtaining sensor events fails.

# 1.1.3

* Fixed detection of mobile devices.

# 1.1.2

* Fixed JSDoc documentation generation.

# 1.1.1

* Fixed npm publishing pipeline.

# 1.1.0

* Performed internal changes to the library.

# 1.0.0

* Initial release.
