export const testData = [
  {
    defaultConfig: {
      inputs: ['cursor', 'wheel'],
      logToConsole: false,
      sensorConfig: {
        sensor_accelerometer: {
          frequency: 60,
        },
        sensor_gyroscope: {
          frequency: 60,
        },
      },
    },
    config: null,
    expectedConfig: {
      inputs: ['cursor', 'wheel'],
      logToConsole: false,
      sensorConfig: {
        sensor_accelerometer: {
          frequency: 60,
        },
        sensor_gyroscope: {
          frequency: 60,
        },
      },
    },
  },
  {
    defaultConfig: {
      inputs: ['cursor', 'wheel'],
      logToConsole: false,
      sensorConfig: {
        sensor_accelerometer: {
          frequency: 60,
        },
        sensor_gyroscope: {
          frequency: 60,
        },
      },
    },
    config: {
      inputs: ['cursor'],
      logToConsole: true,
    },
    expectedConfig: {
      inputs: ['cursor'],
      logToConsole: true,
      sensorConfig: {
        sensor_accelerometer: {
          frequency: 60,
        },
        sensor_gyroscope: {
          frequency: 60,
        },
      },
    },
  },
  {
    defaultConfig: {
      inputs: ['cursor', 'wheel'],
      logToConsole: false,
      sensorConfig: {
        sensor_accelerometer: {
          frequency: 60,
          anotherValue: 20,
        },
        sensor_gyroscope: {
          frequency: 60,
        },
      },
    },
    config: {
      sensorConfig: {
        sensor_accelerometer: {
          frequency: 10,
        },
      },
    },
    expectedConfig: {
      inputs: ['cursor', 'wheel'],
      logToConsole: false,
      sensorConfig: {
        sensor_accelerometer: {
          frequency: 10,
          anotherValue: 20,
        },
        sensor_gyroscope: {
          frequency: 60,
        },
      },
    },
  },
];
