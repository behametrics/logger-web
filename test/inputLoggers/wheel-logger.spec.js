import chai from 'chai';
import {getDefaultConfig} from '../../src/config';
import {WheelLogger} from '../../src/inputLoggers/wheel-logger';
import {INPUT_CURSOR, INPUT_WHEEL} from '../../src/inputLoggers/input-names';
import {testEvents} from './wheel-logger-test-suite';

const expect = chai.expect;
let logger;

describe('Given an instance of WheelLogger', () => {
  before(() => {
    logger = new WheelLogger(getDefaultConfig());
  });

  describe('When a WheelLogger instance is created', () => {
    it('the instance should be defined', () => {
      expect(logger).not.to.be.undefined;
    });
  });

  testEvents.forEach((eventData) => {
    describe('When wheel events are obtained', () => {
      it('the events should contain expected data', () => {
        const event = {
          type: 'wheel',
          timeStamp: 456,
          screenX: 5,
          screenY: 5,
          deltaX: 0,
          deltaY: 10,
          deltaZ: 0,
          deltaMode: 1,
          ...eventData.event,
        };

        const expectedResult = [];

        eventData.expectedEventsContent.forEach((element) => {
          expectedResult.push({
            input: INPUT_WHEEL,
            timestamp: 456,
            content: {
              delta_x: 0,
              delta_y: 10,
              delta_z: 0,
              delta_unit: 'line',
              cursor_x: 5,
              cursor_y: 5,
              ...element,
            },
          });
        });

        expect(logger.getWheelEvents(event)).to.deep.equal(expectedResult);
      });
    });
  });
});
