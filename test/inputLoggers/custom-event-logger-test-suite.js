import {INPUT_CUSTOM_EVENT} from '../../src/inputLoggers/input-names';

export const testEvents = [
  {
    input: INPUT_CUSTOM_EVENT,
    timestamp: 456,
    content: {
      username: 'John Doe',
      element_clicked: '',
    },
  },
  {
    input: 'a_different_input_name',
    timestamp: 456,
    content: {
    },
  },
];
