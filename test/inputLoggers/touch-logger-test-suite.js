export const testEvents = [
  {
    type: 'touchstart',
    touches: getTouches([
      {
        identifier: 0,
      },
    ]),
    expectedEventsContent: [
      {
        event_type: 'down',
        event_type_detail: 'down',
        pointer_id: 0,
      },
    ],
  },
  {
    type: 'touchend',
    touches: getTouches([
      {
        identifier: 0,
      },
    ]),
    expectedEventsContent: [
      {
        event_type: 'up',
        event_type_detail: 'up',
        pointer_id: 0,
      },
    ],
  },
  {
    type: 'touchmove',
    touches: getTouches([
      {
        identifier: 0,
      },
    ]),
    expectedEventsContent: [
      {
        event_type: 'move',
        event_type_detail: 'move',
        pointer_id: 0,
      },
    ],
  },
  {
    type: 'touchcancel',
    touches: getTouches([
      {
        identifier: 0,
      },
    ]),
    expectedEventsContent: [
      {
        event_type: 'cancel',
        event_type_detail: 'cancel',
        pointer_id: 0,
      },
    ],
  },
  {
    type: 'unknown',
    touches: getTouches([
      {
        identifier: 0,
      },
    ]),
    expectedEventsContent: [
      {
        event_type: 'other',
        event_type_detail: 'unknown',
        pointer_id: 0,
      },
    ],
  },
  {
    type: 'touchmove',
    touches: getTouches([
      {
        identifier: 0,
      },
      {
        identifier: 1,
      },
    ]),
    expectedEventsContent: [
      {
        event_type: 'move',
        event_type_detail: 'move',
        pointer_id: 0,
      },
      {
        event_type: 'move',
        event_type_detail: 'move',
        pointer_id: 1,
      },
    ],
  },
];

function getTouches(array) {
  array.item = (index) => array[index];

  return array;
}
