export const testEvents = [
  {
    event: {
      type: 'keydown',
      code: 'a',
      key: 'a',
    },
    expectedContent: {
      event_type: 'down',
      key_code: 'a',
      key_name: 'a',
    },
  },
  {
    event: {
      type: 'keyup',
      code: 'a',
      key: 'a',
    },
    expectedContent: {
      event_type: 'up',
      key_code: 'a',
      key_name: 'a',
    },
  },
  {
    event: {
      type: 'keydown',
      code: 'Numpad4',
      key: '4',
    },
    expectedContent: {
      event_type: 'down',
      key_code: 'Numpad4',
      key_name: '4',
    },
  },
];
