import chai from 'chai';
import sinon from 'sinon';
import {getDefaultConfig} from '../../src/config';
import {CustomEventLogger} from '../../src/inputLoggers/custom-event-logger';
import {testEvents} from './custom-event-logger-test-suite';

const expect = chai.expect;
let logger;

describe('Given an instance of CustomEventLogger', () => {
  before(() => {
    logger = new CustomEventLogger(getDefaultConfig());
  });

  beforeEach(() => {
    sinon.stub(window.performance, 'now').returns(456);
  });

  afterEach(() => {
    window.performance.now.restore();
  });

  describe('When a CustomEventLogger instance is created', () => {
    it('the instance should be defined', () => {
      expect(logger).not.to.be.undefined;
    });
  });

  testEvents.forEach((eventData) => {
    describe(`When a custom event is obtained`, () => {
      it(`the event should contain custom data in expected format`, () => {
        const expectedResult = eventData;

        const result = logger.getCustomEvent(eventData.content, eventData.input);

        expect(result).to.deep.equal(expectedResult);
      });
    });
  });
});
